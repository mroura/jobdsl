project_name = "jenkins-pipeline-python"
repo = "https://gitlab.com/mroura/test-jenkins-publico.git"
repo_name = "repo"

pipelineJob(){
	definition{
		triggers{
			scm('H/1 * * * *')
		}
		
		cpsScm{
			scm{
				git{
					remote{
						uname(repo_name)
						url(repo)
					}
				}
				scriptPath("JenkinsFile")
			}
		}
	}

}